**NOTICE! A git repository containing a more recent version of this code, compilable, testable and executable with Maven, can be found on [GitHub](https://github.com/ecos-umons/TimerStopwatch)**

---
# README #


This repository contains a simple example used for teaching purposes in a software modelling course by Tom Mens at the University of Mons.

The example is an event-based application for a timer and stopwatch. It can be controlled through 3 buttons/events.

The examples comes with UML models (created with Visual Paradigm), executable statechart models (created with Yakindu), Java 8 source code, and JUnit tests.

The aim of the example is to illustrate how to model composite statecharts, and how to implement them in Java in object-oriented style using the State Design Pattern.

## Video Tutorials ##

See https://www.youtube.com/watch?v=5uYOPUj5J3Q
and https://www.youtube.com/watch?v=Qwy7Gpw3YQc
for two tutorial videos on YouTube that use this example Java source code.

Additional video tutorials that make use of this example can be found on the following [YouTube playlist](https://www.youtube.com/playlist?list=PLmHMvhX5wK_aohX5sOeAMogFDwlc3gJYR)

## Contact ##

[Tom Mens](mailto:tom.mens@umons.ac.be),
Software Engineering Lab,
University of Mons, Belgium